# Coding Challenge

## Goal

Create a TODO web application. Users should be able to register and manage one or multiple list of TODOs. A TODO consists of a text and optionally a geo location or a due date/deadline. TODOs can be completed, archived and deleted. The text is not unique.

## Technology

The software should be composed of a web frontend application, an api backend application and a postgresql database.

### Web Frontend

- [ ] Angular 17
- [ ] Tailwind CSS
- [ ] Jest
- [ ] Cypress

### API Backend

- [ ] Go
- [ ] Gin
- [ ] logrus
- [ ] viper

## Tasks

- [ ] Fork this repository
- [ ] Use  `compose watch` for development
- [ ] Use a Containerfile to containerize the applications
- [ ] Secure the Container (e.g. non-root user, read-only filesystem, etc.)
- [ ] Supply a `compose.yaml` to start the complete setup (frontend, backend, database) from the containers NOT the source code
- [ ] Testing (unit, integration) of the applications
- [ ] Use JWT for authentication
- [ ] Use conenvtional commits and semantic release (npm package)
- [ ] Use a Gitlab CI/CD pipeline to test (unit, integration), build (Angular, Go binary), package (container), upload (container registry), release (Gitlab Releases) the applications
- [ ] Releases should be tagged and can container test reports, source code, build artefacts, links to the container images etc.
- [ ] Perform container sizing
- [ ] Supply a `helm` chart to deploy the applications to a Kubernetes cluster
- [ ] Support for passkey authentication
- [ ] Deploy the application and make it accessible via a public URL

## Links

- [Angular](https://angular.dev/)
- [Reactive Reified Programming and State Management](https://www.youtube.com/@JoshuaMorony)
- [Typescript Wizard](https://www.youtube.com/results?search_query=matt+pocock)